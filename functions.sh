#!/usr/bin/env bash

# MODULE GOOGLE-DIRECTIONS

# $1 => origine
# $2 => destination
# $3 => true/false
# $4 => argument
js_gd_getDirection(){

    if [[ -n "$js_gd_apiKey" ]]
    then
        restrictions=$(js_ia_getStrictText destinationconfig 2)
        adresseOrigine=$(js_ia_getStrictText destination$1 3)
        adresseDestination=$(js_ia_getStrictText destination$2 3)

        json=$(curl -s -G "https://maps.googleapis.com/maps/api/directions/json" --data "language=fr" --data-urlencode "origin=$adresseOrigine" --data-urlencode "destination=$adresseDestination" --data "traffic_model=best_guess" --data "avoid=$restrictions" --data "departure_time=now" --data-urlencode "key=$js_gd_apiKey")
        tempTrajetNormalSecondes=$(echo "$json" | jq -r '.routes[0].legs[0].duration.value')

        if [ $tempTrajetNormalSecondes = 'null' ]
        then
            if [ $3 = 'true' ]
            then
                js_ia_say "erreur"
                say "Je n'ai pas compris votre destination"
            else
                js_ia_sendToMe "Je n'ai pas compris votre destination"
            fi
        else
            tempTrajetTrafficSecondes=$(echo "$json" | jq -r '.routes[0].legs[0].duration_in_traffic.value')
            tempTrajetTrafficLisible=$(echo "$json" | jq -r '.routes[0].legs[0].duration_in_traffic.text')
            routesPrisent=$(echo "$json" | jq -r '.routes[0].summary')
            retard=$((($tempTrajetTrafficSecondes-$tempTrajetNormalSecondes)/60))
            #Timestamp actuel
            timeStampActuel=$(date +%s)
            #time de l'heure d'arrivée
            heureArriveeSecondes=$(($timeStampActuel+$tempTrajetTrafficSecondes))
            #conversion du Timestamp en lisible
            heureArriveeLisible=$(date -d @$heureArriveeSecondes +'%H:%M')
            #reccupere l'année le mois et le jour actuel
            anneeMoisJour=$(date +%Y/%m/%d)
            #Timestamp de la limite d'heure
            heureLimiteDestination=$(js_ia_getStrictText destination$2 4)
            heureLimiteArriveeSecondes=$(date -d "$anneeMoisJour $heureLimiteDestination" +'%s')

            resteAvantDepartSecondes=$(($heureLimiteArriveeSecondes-$heureArriveeSecondes))
            resteAvantDepartLisible=$((($resteAvantDepartSecondes)/60))
            heureAvantDepartSeconde=$(($timeStampActuel+$resteAvantDepartSecondes))
            heureAvantDepartLisible=$(date -d @$heureAvantDepartSeconde +'%H:%M')

            nomDestination=$(js_ia_getStrictText destination$2 2)

            if [ $4 = 'temps' ]
            then
                if [ $3 = 'true' ]
                then
                    say "Le temps de trajet est estimé à $tempTrajetTrafficLisible comprenant $retard minutes de retard"
                else
                    js_ia_sendToMe "Le temps de trajet est estimé à $tempTrajetTrafficLisible comprenant $retard minutes de retard"
                fi

            elif [ $4 = 'heurearrivee' ]
            then
                if [ $3 = 'true' ]
                then
                    say "Vous arriverez $nomDestination à $heureArriveeLisible"
                else
                    js_ia_sendToMe "Vous arriverez $nomDestination à $heureArriveeLisible"
                fi

            elif [ $4 = 'retard' ]
            then
                if [ $3 = 'true' ]
                then
                    say "il faut compter un retard de $retard minutes sur le temps de trajet habituel"
                else
                    js_ia_sendToMe "il faut compter un retard de $retard minutes sur le temps de trajet habituel"
                fi

            elif [ $4 = 'limite' ]
            then
                if [ $3 = 'true' ]
                then
                    say "Pour arriver à $heureLimiteDestination $nomDestination vous devez partir dans les $resteAvantDepartLisible minutes soit au plus tard à $heureAvantDepartLisible"
                else
                    js_ia_sendToMe "Pour arriver à $heureLimiteDestination $nomDestination vous devez partir dans les $resteAvantDepartLisible minutes soit au plus tard à $heureAvantDepartLisible"
                fi

            elif [ $4 = 'debug' ]
            then
                say "tempTrajetNormalSecondes $tempTrajetNormalSecondes"
                say "tempTrajetTrafficSecondes $tempTrajetTrafficSecondes"
                say "tempTrajetTrafficLisible $tempTrajetTrafficLisible"
                say "routesPrisent $routesPrisent"
                say "retard $retard"
                say "timeStampActuel $timeStampActuel"
                say "heureArriveeSecondes $heureArriveeSecondes"
                say "heureArriveeLisible $heureArriveeLisible"
                say "anneeMoisJour $anneeMoisJour"
                say "heureLimiteArriveeSecondes $heureLimiteArriveeSecondes"
                say "resteAvantDepartSecondes $resteAvantDepartSecondes"
                say "resteAvantDepartLisible $resteAvantDepartLisible"
            else
                if [ $3 = 'true' ]
                then
                    say "Le temps de trajet est estimé à $tempTrajetTrafficLisible comprenant $retard minutes de retard, vous arriverez $nomDestination à $heureArriveeLisible"
                else
                    js_ia_sendToMe "Le temps de trajet est estimé à $tempTrajetTrafficLisible comprenant $retard minutes de retard, vous arriverez $nomDestination à $heureArriveeLisible"
                fi
            fi
        fi

    else
        js_ia_say "erreur"
        say "La clef google Directions est invalide"
    fi
}
